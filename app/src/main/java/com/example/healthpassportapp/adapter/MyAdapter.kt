package com.example.healthpassportapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.healthpassportapp.R
import com.example.healthpassportapp.model.UserInfo

class MyAdapter(private val mDataList: ArrayList<UserInfo>) :
    RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.parameterKey.text = mDataList[position].parameterKey
        holder.parameterValue.text = mDataList[position].value
    }

    override fun getItemCount(): Int {
        return mDataList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var parameterKey: TextView =
            itemView.findViewById<View>(R.id.parameter_key) as TextView
        internal var parameterValue: TextView =
            itemView.findViewById<View>(R.id.parameter_value) as TextView
    }
}