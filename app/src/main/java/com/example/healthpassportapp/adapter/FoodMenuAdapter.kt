package com.example.healthpassportapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.TextView
import com.example.healthpassportapp.R
import com.example.healthpassportapp.model.MenuData

class FoodMenuAdapter(context: Context, var menuList: ArrayList<MenuData>) : BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View
        val vh: ItemHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item, parent, false)
            vh = ItemHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }

        vh.textMenuName.text = menuList[position].menu
        if (position == 0) {
            vh.checkbox.visibility = View.INVISIBLE
        }
        vh.checkbox.setOnCheckedChangeListener { _, isChecked ->
            menuList[position].isSelected = isChecked
        }
        return view
    }

    override fun getItem(position: Int): Any {
        return menuList[position]
    }

    override fun getCount(): Int {
        return menuList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private class ItemHolder(row: View?) {
        val textMenuName: TextView = row?.findViewById(R.id.textMenuName) as TextView
        val checkbox: CheckBox = row?.findViewById(R.id.checkbox) as CheckBox
    }
}