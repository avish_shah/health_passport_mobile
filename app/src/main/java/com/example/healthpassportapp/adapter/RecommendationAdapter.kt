package com.example.healthpassportapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.healthpassportapp.R
import com.example.healthpassportapp.model.Recommendation

class RecommendationAdapter(private val mRecommendationList: ArrayList<Recommendation>?) :
    RecyclerView.Adapter<RecommendationAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater
            .from(parent.context).inflate(R.layout.recommendation_view, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.parameterKey.text = mRecommendationList?.get(position)?.title
        holder.parameterValue.text = mRecommendationList?.get(position)?.message
    }

    override fun getItemCount(): Int {
        if (mRecommendationList != null) {
            return mRecommendationList.size
        }
        return 0
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var parameterKey: TextView =
            itemView.findViewById<View>(R.id.parameter_key) as TextView
        internal var parameterValue: TextView =
            itemView.findViewById<View>(R.id.parameter_value) as TextView
    }
}