package com.example.healthpassportapp

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.example.healthpassportapp.adapter.FoodMenuAdapter
import com.example.healthpassportapp.model.MenuData
import com.example.healthpassportapp.utils.PreferenceUtil
import java.util.Calendar

class HealthInputDataActivity : AppCompatActivity() {

    private val sharedPrefFile = "sharedpreference"
    private lateinit var sharedPreferences: SharedPreferences

    private lateinit var btnShareData: Button
    private lateinit var radioGroupAlcohol: RadioGroup
    private lateinit var radioGroupSoftDrink: RadioGroup
    private lateinit var radioGroupSmoke: RadioGroup

    private lateinit var edtAlcoholValue: EditText
    private lateinit var edtSoftDrinkValue: EditText
    private lateinit var edtSmokeValue: EditText

    private lateinit var spinnerBreakfast: Spinner
    private lateinit var spinnerLunch: Spinner
    private lateinit var spinnerDinner: Spinner

    private var alcoholValue: Int = 0
    private var softDrinkValue: Int = 0
    private var smokeValue: Int = 0

    private val foodArray = arrayOf(
        "Select food", "Sprouts", "Dal & Rice", "Apple", "Orange Juice", "Pizza & 1 Bowl Pasta",
        "Salad", "Noodles & Fried Rice", "Chocolate", "3 Chapati & Curry"
    )
    private var foodBreakfastMenuList: ArrayList<MenuData> = ArrayList()
    private var foodLunchMenuList: ArrayList<MenuData> = ArrayList()
    private var foodDinnerMenuList: ArrayList<MenuData> = ArrayList()

    private var intakeBreakfastMenu: ArrayList<String> = ArrayList()
    private var intakeLunchMenu: ArrayList<String> = ArrayList()
    private var intakeDinnerMenu: ArrayList<String> = ArrayList()

    private var breakfastCalories: Int = 0
    private var lunchCalories: Int = 0
    private var dinnerFastCalories: Int = 0
    private var totalIntakeCalories: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_input_data)
        initializeView()
    }

    private fun initializeView() {
        sharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        btnShareData = findViewById(R.id.btnShareData)

        radioGroupAlcohol = findViewById(R.id.radioGroupAlcohol)
        radioGroupSoftDrink = findViewById(R.id.radioGroupSoftDrink)
        radioGroupSmoke = findViewById(R.id.radioGroupSmoke)

        edtAlcoholValue = findViewById(R.id.edtAlcoholValue)
        edtSoftDrinkValue = findViewById(R.id.edtSoftDrinkValue)
        edtSmokeValue = findViewById(R.id.edtSmokeValue)

        spinnerBreakfast = findViewById(R.id.spinnerBreakfast)
        spinnerLunch = findViewById(R.id.spinnerLunch)
        spinnerDinner = findViewById(R.id.spinnerDinner)

        for (i in foodArray.indices) {
            val menuData = MenuData()
            menuData.menu = (foodArray[i])
            menuData.isSelected = false
            foodBreakfastMenuList.add(menuData)
        }

        for (i in foodArray.indices) {
            val menuData = MenuData()
            menuData.menu = (foodArray[i])
            menuData.isSelected = false
            foodLunchMenuList.add(menuData)
        }

        for (i in foodArray.indices) {
            val menuData = MenuData()
            menuData.menu = (foodArray[i])
            menuData.isSelected = false
            foodDinnerMenuList.add(menuData)
        }

        val foodBreakfastMenuAdapter = FoodMenuAdapter(this, foodBreakfastMenuList)
        spinnerBreakfast.adapter = foodBreakfastMenuAdapter

        val foodLunchMenuAdapter = FoodMenuAdapter(this, foodLunchMenuList)
        spinnerLunch.adapter = foodLunchMenuAdapter

        val foodDinnerMenuAdapter = FoodMenuAdapter(this, foodDinnerMenuList)
        spinnerDinner.adapter = foodDinnerMenuAdapter

        radioGroupAlcohol.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.radioYes -> edtAlcoholValue.visibility = View.VISIBLE
                R.id.radioNo -> {
                    edtAlcoholValue.visibility = View.GONE
                    edtAlcoholValue.setText("0")
                }
                else -> edtAlcoholValue.visibility = View.GONE
            }
        }

        radioGroupSoftDrink.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radioSoftDrinkYes -> edtSoftDrinkValue.visibility = View.VISIBLE
                R.id.radioSoftDrinkNo -> {
                    edtSoftDrinkValue.visibility = View.GONE
                    edtSoftDrinkValue.setText("0")
                }
                else -> edtSoftDrinkValue.visibility = View.GONE
            }
        }

        radioGroupSmoke.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radioSmokeYes -> {
                    edtSmokeValue.visibility = View.VISIBLE
                }
                R.id.radioSmokeNo -> {
                    edtSmokeValue.visibility = View.GONE
                    edtSmokeValue.setText("0")
                }
                else -> edtSmokeValue.visibility = View.GONE
            }
        }

        btnShareData.setOnClickListener {

            if (!edtAlcoholValue.text.trim().toString().isEmpty()) {
                alcoholValue = Integer.parseInt(edtAlcoholValue.text.toString())
            }

            if (!edtSoftDrinkValue.text.trim().toString().isEmpty()) {
                softDrinkValue = Integer.parseInt(edtSoftDrinkValue.text.toString())
            }
            if (!edtSmokeValue.text.trim().toString().isEmpty()) {
                smokeValue = Integer.parseInt(edtSmokeValue.text.toString())
            }
            totalIntakeCalories = 0

            getBreakfastCalories()
            getLunchCalories()
            getDinnerCalories()

            totalIntakeCalories = breakfastCalories + lunchCalories + dinnerFastCalories

            PreferenceUtil().storeIntDataInPreference(this, PreferenceUtil.ALCOHOL, alcoholValue)
            PreferenceUtil().storeIntDataInPreference(
                this,
                PreferenceUtil.SOFT_DRINK,
                softDrinkValue
            )
            PreferenceUtil().storeIntDataInPreference(this, PreferenceUtil.SMOKE, smokeValue)
            PreferenceUtil().storeIntDataInPreference(
                this,
                PreferenceUtil.TOTAL_CALORIES,
                totalIntakeCalories
            )
            PreferenceUtil().storeLongDataInPreference(
                this.baseContext,
                PreferenceUtil.LAST_DISPLAY_TIME,
                Calendar.getInstance().timeInMillis
            )

            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun getBreakfastCalories() {
        //get selected breakfast menu from list
        breakfastCalories = 0
        for (menuData in foodBreakfastMenuList) {
            if (menuData.isSelected) {
                intakeBreakfastMenu.add(menuData.menu)
            }
        }
        breakfastCalories = calculateCalories(intakeBreakfastMenu)
    }

    private fun getLunchCalories() {
        //get selected breakfast menu from list
        lunchCalories = 0
        for (menuData in foodLunchMenuList) {
            if (menuData.isSelected) {
                intakeLunchMenu.add(menuData.menu)
            }
        }
        lunchCalories = calculateCalories(intakeLunchMenu)
    }

    private fun getDinnerCalories() {
        //get selected breakfast menu from list
        dinnerFastCalories = 0
        for (menuData in foodDinnerMenuList) {
            if (menuData.isSelected) {
                intakeDinnerMenu.add(menuData.menu)
            }
        }
        dinnerFastCalories = calculateCalories(intakeDinnerMenu)
    }

    private fun calculateCalories(breakfastMenu: ArrayList<String>): Int {
        var totalCalories = 0
        var calS = 0
        var calDR = 0
        var calA = 0
        var calOJ = 0
        var calPP = 0
        var calSL = 0
        var calNF = 0
        var calC = 0
        var calCC = 0
        for (intakeBreakfastMenu in breakfastMenu) {
            if (intakeBreakfastMenu == "Sprouts") {
                calS = MenuWithCalories.SPROUTS.numericType
            }
            if (intakeBreakfastMenu == "Dal & Rice") {
                calDR = MenuWithCalories.DAL_RICE.numericType
            }
            if (intakeBreakfastMenu == "Apple") {
                calA = MenuWithCalories.APPLE.numericType
            }
            if (intakeBreakfastMenu == "Orange Juice") {
                calOJ = MenuWithCalories.ORANGE_JUICE.numericType
            }
            if (intakeBreakfastMenu == "Pizza & 1 Bowl Pasta") {
                calPP = MenuWithCalories.PIZZA_1BOWL_PASTA.numericType
            }
            if (intakeBreakfastMenu == "Salad") {
                calSL = MenuWithCalories.SALAD.numericType
            }
            if (intakeBreakfastMenu == "Noodles & Fried Rice") {
                calNF = MenuWithCalories.NOODLES_FRIED_RICE.numericType
            }
            if (intakeBreakfastMenu == "Chocolate") {
                calC = MenuWithCalories.CHOCOLATE.numericType
            }
            if (intakeBreakfastMenu == "3 Chapati & Curry") {
                calCC = MenuWithCalories.CHAPATI3CURRY.numericType
            }
            totalCalories = calA + calC + calCC + calDR + calNF + calOJ + calPP + calS + calSL
            println("totalCalories BF: $totalCalories")
        }
        return totalCalories
    }
}