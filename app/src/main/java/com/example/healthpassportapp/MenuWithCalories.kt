package com.example.healthpassportapp

enum class MenuWithCalories (val numericType: Int) {
        SPROUTS(43),
        DAL_RICE(390),
        APPLE(81),
        ORANGE_JUICE(112),
        PIZZA_1BOWL_PASTA(2269),
        SALAD(45),
        NOODLES_FRIED_RICE(713),
        CHOCOLATE(500),
        CHAPATI3CURRY(443)
}
