package com.example.healthpassportapp.network

import com.example.healthpassportapp.model.BgResponse
import com.example.healthpassportapp.model.HealthScoreResponse
import com.example.healthpassportapp.model.UserData
import com.example.healthpassportapp.model.UserInfo
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiService {
    companion object {
        const val BASE_URL: String = "http://54.74.29.218:8080/healthPassport/"
    }

    @GET("v1/bgdata/{email}")
    fun getBgData(@Path("email") email: String?): Call<BgResponse>

    @POST("v1/getScoreAndRecommendations")
    fun getHealthScore(@Body userDataRequest: UserData?): Call<HealthScoreResponse>
}