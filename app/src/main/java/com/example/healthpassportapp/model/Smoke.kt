package com.example.healthpassportapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Smoke(
    @Expose
    @SerializedName("consumed")
    var consumed: Boolean,
    @Expose
    @SerializedName("howmuch")
    var howmuch: Int = 0
)