package com.example.healthpassportapp.model

data class UserInfo(var parameterKey: String, var value: String)