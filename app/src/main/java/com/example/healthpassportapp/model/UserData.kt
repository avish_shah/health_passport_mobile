package com.example.healthpassportapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserData(
    @Expose
    @SerializedName("caloryBurned")
    var caloryBurned: Int,
    @Expose
    @SerializedName("caloryConsumed")
    var caloryConsumed: Int,
    @Expose
    @SerializedName("dob")
    var dob: String,
    @Expose
    @SerializedName("email")
    var email: String,
    @Expose
    @SerializedName("gender")
    var gender: String,
    @Expose
    @SerializedName("height")
    var height: Double,
    @Expose
    @SerializedName("weight")
    var weight: Double,
    @Expose
    @SerializedName("sleepHours")
    var sleepHours: Int,
    @Expose
    @SerializedName("smoke")
    var smoke: Smoke,
    @Expose
    @SerializedName("softdrink")
    var softdrink: Softdrink,
    @Expose
    @SerializedName("alcohol")
    var alcohol: Alcohol
)