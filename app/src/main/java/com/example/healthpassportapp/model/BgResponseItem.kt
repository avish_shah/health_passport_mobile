package com.example.healthpassportapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BgResponseItem(
    @Expose
    @SerializedName("bgValue")
    var bgValue: Int = 0,
    @Expose
    @SerializedName("insulinValue")
    var insulinValue: Double = 0.0,
    @Expose
    @SerializedName("mealTag")
    var mealTag: String,
    @Expose
    @SerializedName("readingDate")
    var readingDate: String
)