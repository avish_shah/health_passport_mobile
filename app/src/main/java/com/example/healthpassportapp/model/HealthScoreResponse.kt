package com.example.healthpassportapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HealthScoreResponse(
    @Expose
    @SerializedName("score")
    var score: Int = 0,
    @Expose
    @SerializedName("alcohol")
    var alcohol: String? = null,
    @Expose
    @SerializedName("bloodPressureCheck")
    var bloodPressureCheck: String? = null,
    @Expose
    @SerializedName("diet")
    var diet: String? = null,
    @Expose
    @SerializedName("exercise")
    var exercise: String? = null,
    @Expose
    @SerializedName("saltAndSugarControl")
    var saltAndSugarControl: String? = null,
    @Expose
    @SerializedName("sleep")
    var sleep: String? = null,
    @Expose
    @SerializedName("smoke")
    var smoke: String? = null
)