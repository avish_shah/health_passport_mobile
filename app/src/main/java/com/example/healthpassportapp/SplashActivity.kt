package com.example.healthpassportapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Window

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_ACTION_BAR);
        actionBar?.hide()
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash)
        Handler(Looper.getMainLooper()).postDelayed(
            {
                startActivity(Intent(this, DashboardActivity::class.java))
                this.finish()
            },
            SPLASH_DISPLAY_LENGTH
        )
    }

    companion object {
        const val SPLASH_DISPLAY_LENGTH: Long = 3000
    }
}