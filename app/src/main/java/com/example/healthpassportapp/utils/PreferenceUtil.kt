package com.example.healthpassportapp.utils

import android.content.Context
import android.content.SharedPreferences

class PreferenceUtil {
    private val sharedPrefFile = "sharedPreference"

    fun storeBooleanDataInPreference(context: Context?, preferenceKey: String, value: Boolean) {
        val editor: SharedPreferences.Editor? =
            context?.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)?.edit()
        editor?.putBoolean(preferenceKey, value)
        editor?.apply()
    }

    fun storeIntDataInPreference(context: Context, preferenceKey: String, value: Int) {
        val editor: SharedPreferences.Editor =
            context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE).edit()
        editor.putInt(preferenceKey, value)
        editor.apply()
    }

    fun storeLongDataInPreference(context: Context?, preferenceKey: String, value: Long) {
        val editor: SharedPreferences.Editor? =
            context?.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)?.edit()
        editor?.putLong(preferenceKey, value)
        editor?.apply()
    }

    fun getIntDataFromPreference(context: Context?, preferenceKey: String): Int {
        val preference: SharedPreferences? =
            context?.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        if (preference != null) {
            return preference.getInt(preferenceKey, 0)
        }
        return 0
    }

    fun getLongDataFromPreference(context: Context?, preferenceKey: String): Long {
        val preference: SharedPreferences? =
            context?.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        if (preference != null) {
            return preference.getLong(preferenceKey, 0)
        }
        return 0
    }

    fun getBooleanDataFromPreference(context: Context?, preferenceKey: String): Boolean {
        val preference: SharedPreferences? =
            context?.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        if (preference != null) {
            return preference.getBoolean(preferenceKey, false)
        }
        return false
    }

    companion object {
        const val ALCOHOL = "Alcohol"
        const val SOFT_DRINK = "softDrink"
        const val SMOKE = "smoke"
        const val TOTAL_CALORIES = "totalCalories"
        const val LAST_DISPLAY_TIME = "LastDisplayTime"
        const val IS_POPUP_DISPLAYED = "isPopupDisplayed"
    }
}