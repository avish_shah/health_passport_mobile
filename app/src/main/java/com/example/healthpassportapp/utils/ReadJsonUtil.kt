package com.example.healthpassportapp.utils

import android.content.Context
import com.example.healthpassportapp.model.UserData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException

class ReadJsonUtil {

    private fun readJsonFile(context: Context?, fileName: String): String? {
        val jsonString: String?
        try {
            jsonString = context?.assets?.open(fileName)?.bufferedReader().use { it?.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    fun getMockDataValueFromJson(context: Context?, fileName: String): UserData {
        val jsonFileString = readJsonFile(context, fileName)

        val gson = Gson()
        val listPersonType = object : TypeToken<UserData>() {}.type

        return gson.fromJson(jsonFileString, listPersonType)
    }
}