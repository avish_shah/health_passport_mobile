package com.example.healthpassportapp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.healthpassportapp.R
import com.example.healthpassportapp.adapter.RecommendationAdapter
import com.example.healthpassportapp.model.Recommendation

class RecommendationFragment : Fragment() {
    private var recyclerView: RecyclerView? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.recommendation_fragment, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recommendationArray: ArrayList<Recommendation>? =
            arguments?.getParcelableArrayList("data")

        val mLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView!!.layoutManager = mLayoutManager
        mAdapter = RecommendationAdapter(recommendationArray)
        recyclerView!!.adapter = mAdapter
    }

    companion object {
        fun newInstance(recommendation: ArrayList<Recommendation>?): RecommendationFragment {
            val fragment = RecommendationFragment()
            val args = Bundle()
            args.putParcelableArrayList("data", recommendation)
            fragment.arguments = args
            return fragment
        }
    }
}