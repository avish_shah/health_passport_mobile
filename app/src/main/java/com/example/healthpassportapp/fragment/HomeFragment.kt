package com.example.healthpassportapp.fragment

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.healthpassportapp.DashboardActivity
import com.example.healthpassportapp.HealthInputDataActivity
import com.example.healthpassportapp.R
import com.example.healthpassportapp.model.UserInfo
import com.example.healthpassportapp.adapter.MyAdapter
import com.example.healthpassportapp.model.BgResponse
import com.example.healthpassportapp.model.HealthScoreResponse
import com.example.healthpassportapp.model.Recommendation
import com.example.healthpassportapp.model.UserData
import com.example.healthpassportapp.network.ApiProvider
import com.example.healthpassportapp.network.ApiService
import com.example.healthpassportapp.utils.PreferenceUtil
import com.example.healthpassportapp.utils.ReadJsonUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException

import com.rtugeek.android.colorseekbar.ColorSeekBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.Calendar
import java.util.concurrent.TimeUnit

class HomeFragment : Fragment() {
    private lateinit var cardView: CardView
    private var mColorSeekBar: ColorSeekBar? = null
    private var recyclerView: RecyclerView? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null
    private var mScoreValue: TextView? = null
    private var mScoreStatus: TextView? = null
    private var listOfUsers: ArrayList<UserInfo> = ArrayList()
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private var dialog: Dialog? = null
    private var account: GoogleSignInAccount? = null
    private var bgResponse: BgResponse? = null
    private lateinit var apiService: ApiService
    private lateinit var userData: UserData
    private var recommendationList: ArrayList<Recommendation>? = null
    private var alcoholValue = 0
    private var smokeValue = 0
    private var softDrinkValue = 0
    private var caloryConsumed = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_fragment, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        mColorSeekBar = view.findViewById(R.id.colorSlider)
        cardView = view.findViewById(R.id.cardView)
        mColorSeekBar?.setColorSeeds(R.array.text_colors)
        mScoreValue = view.findViewById(R.id.health_score_value)
        mScoreStatus = view.findViewById(R.id.healthScoreRemark)
        mScoreValue?.text = "Get Score"
        cardView.setOnClickListener {
            (activity as DashboardActivity).openSelectedFragment()
            val nextFrag = RecommendationFragment.newInstance(recommendationList)
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.content, nextFrag, nextFrag.javaClass.simpleName)
                ?.addToBackStack(null)
                ?.commit()
        }

        mScoreValue?.setOnClickListener {
            if (this::userData.isInitialized) {
                getHealthScore(userData)
            }
        }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        apiService = ApiProvider.createService(ApiService::class.java)
        displayAlertForGoogleFitConnect()
    }

    private fun displayAlertForGoogleFitConnect() {
        if (!PreferenceUtil().getBooleanDataFromPreference(
                activity?.baseContext,
                PreferenceUtil.IS_POPUP_DISPLAYED
            )
        ) {
            val builder = AlertDialog.Builder(activity)
                .setTitle(R.string.connect_with_google_fit_title)
                .setMessage(R.string.connect_with_google_fit_message)
                .setPositiveButton("Yes") { _, _ ->
                    signInWithGoogle()
                    PreferenceUtil().storeBooleanDataInPreference(
                        activity?.baseContext,
                        PreferenceUtil.IS_POPUP_DISPLAYED,
                        true
                    )
                }.setNegativeButton("No") { _, _ ->

                }
            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
        } else {
            signInWithGoogle()
        }
    }

    private fun checkToDisplayDailyDataActivity() {
        val duration: Long =
            Calendar.getInstance().timeInMillis - PreferenceUtil().getLongDataFromPreference(
                activity?.baseContext,
                PreferenceUtil.LAST_DISPLAY_TIME
            )
        if (PreferenceUtil().getBooleanDataFromPreference(
                activity?.baseContext,
                PreferenceUtil.IS_POPUP_DISPLAYED
            ) && TimeUnit.MILLISECONDS.toDays(duration) >= 1
        ) {
            val intent = Intent(context, HealthInputDataActivity::class.java)
            activityLauncher.launch(intent)
        } else {
            getBgRecord(account?.email)
        }
    }

    private var activityLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                getBgRecord(account?.email)
            }
        }

    private fun displayUserDataInDashboard() {
        userData = getUserInformation()

        listOfUsers.add(UserInfo("Calories Burned (kCal)", userData.caloryBurned.toString()))
        listOfUsers.add(UserInfo("Sleep (Hours)", userData.sleepHours.toString()))
        listOfUsers.add(UserInfo("BMI value (kg/m2)", getBMI(userData.weight, userData.height)))

        if (bgResponse != null) {
            listOfUsers.add(UserInfo("BG value (mg/dl)", bgResponse?.get(0)?.bgValue.toString()))
            listOfUsers.add(
                UserInfo(
                    "Insulin value (u)",
                    bgResponse?.get(0)?.insulinValue.toString()
                )
            )
        }

        caloryConsumed = PreferenceUtil().getIntDataFromPreference(
            activity?.baseContext,
            PreferenceUtil.TOTAL_CALORIES
        )
        if (caloryConsumed != 0) {
            listOfUsers.add(UserInfo("Calories Consumed (kCal)", caloryConsumed.toString()))
        }

        smokeValue = PreferenceUtil().getIntDataFromPreference(
            activity?.baseContext,
            PreferenceUtil.SMOKE
        )
        if (smokeValue != 0) {
            listOfUsers.add(UserInfo("Smoking", smokeValue.toString()))
        }

        alcoholValue = PreferenceUtil().getIntDataFromPreference(
            activity?.baseContext,
            PreferenceUtil.ALCOHOL
        )
        if (alcoholValue != 0) {
            listOfUsers.add(UserInfo("Alcohol Consumption (ml)", alcoholValue.toString()))
        }

        softDrinkValue = PreferenceUtil().getIntDataFromPreference(
            activity?.baseContext,
            PreferenceUtil.SOFT_DRINK
        )
        if (softDrinkValue != 0) {
            listOfUsers.add(UserInfo("Soft Drink consumption (ml)", softDrinkValue.toString()))
        }

        val mLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView!!.layoutManager = mLayoutManager
        mAdapter = MyAdapter(listOfUsers)
        recyclerView!!.adapter = mAdapter
        setSeekBar()
    }

    private fun getBMI(weight: Double, height: Double): String {
        val value: Double = weight / (height * height) * 10000
        return BigDecimal(value).setScale(2, RoundingMode.HALF_EVEN).toString()
    }

    private fun getUserInformation(): UserData {
        val fileName: String = when (account?.email) {
            "johndoemailinator5@gmail.com" -> {
                "MockData1.json"
            }
            "johnsmith.hp2021@gmail.com" -> {
                "MockData2.json"
            }
            else -> {
                "MockData3.json"
            }
        }
        return ReadJsonUtil().getMockDataValueFromJson(activity?.baseContext, fileName)
    }

    private fun displayProgress() {
        val builder = AlertDialog.Builder(activity)
        builder.setView(R.layout.progress_dialog)
        dialog = builder.create()
        dialog?.show()
    }

    private fun hideProgress() {
        dialog?.dismiss()
    }

    private fun getBgRecord(email: String?) {
        displayProgress()
        apiService.getBgData(email).enqueue(object : Callback<BgResponse> {
            override fun onResponse(call: Call<BgResponse>?, response: Response<BgResponse>?) {
                if (response?.isSuccessful == true) {
                    if (response.body() != null) {
                        bgResponse = response.body()!!
                    }
                }
                hideProgress()
                displayUserDataInDashboard()
            }

            override fun onFailure(call: Call<BgResponse>?, t: Throwable?) {
                Log.d("HealthDataService", "Error in API: " + t?.message)
                hideProgress()
                displayUserDataInDashboard()
            }
        })
    }

    private fun getHealthScore(userData: UserData?) {
        val userDataRequest = userData
        userDataRequest?.alcohol?.consumed = alcoholValue != 0
        userDataRequest?.smoke?.consumed = smokeValue != 0
        userDataRequest?.softdrink?.consumed = softDrinkValue != 0

        userDataRequest?.alcohol?.howmuch = alcoholValue
        userDataRequest?.smoke?.howmuch = smokeValue
        userDataRequest?.softdrink?.howmuch = softDrinkValue

        if (caloryConsumed != 0) {
            userDataRequest?.caloryConsumed = caloryConsumed
        }

        displayProgress()
        apiService.getHealthScore(userDataRequest).enqueue(object : Callback<HealthScoreResponse> {
            override fun onResponse(
                call: Call<HealthScoreResponse>?,
                response: Response<HealthScoreResponse>?
            ) {
                if (response?.isSuccessful == true) {
                    if (response.body() != null) {
                        mScoreValue?.text = response.body()?.score.toString()
                        mColorSeekBar?.colorBarPosition = response.body()?.score ?: 0
                        getScoreStatus(response.body()?.score?:0)
                        storeRecommendation(response.body())
                    }
                }
                hideProgress()
            }

            override fun onFailure(call: Call<HealthScoreResponse>?, t: Throwable?) {
                Log.d("HealthDataService", "Error in API: " + t?.message)
                hideProgress()
            }
        })
    }

    fun getScoreStatus(scoreCount: Int) {
        when {
            scoreCount <= 25 -> {
                mScoreStatus?.text = "Poor"
            }
            scoreCount <= 50 -> {
                mScoreStatus?.text = "Need attention"
            }
            scoreCount <= 75 -> {
                mScoreStatus?.text = "Average"
            }
            else -> {
                mScoreStatus?.text = "Excellent"
            }
        }
    }

    private fun storeRecommendation(healthScoreResponse: HealthScoreResponse?) {
        recommendationList = ArrayList()
        if (healthScoreResponse?.alcohol != null) {
            recommendationList?.add(
                Recommendation(
                    "Alcohol Consumption Alert",
                    healthScoreResponse.alcohol.toString()
                )
            )
        }
        if (healthScoreResponse?.sleep != null) {
            recommendationList?.add(
                Recommendation(
                    "Sleep Alert",
                    healthScoreResponse.sleep.toString()
                )
            )
        }
        if (healthScoreResponse?.diet != null) {
            recommendationList?.add(
                Recommendation(
                    "Your Diet Need Attention",
                    healthScoreResponse.diet.toString()
                )
            )
        }
        if (healthScoreResponse?.exercise != null) {
            recommendationList?.add(
                Recommendation(
                    "Exercise Alert",
                    healthScoreResponse.exercise.toString()
                )
            )
        }
        if (healthScoreResponse?.saltAndSugarControl != null) {
            recommendationList?.add(
                Recommendation(
                    "Salt And Suger Alert",
                    healthScoreResponse.saltAndSugarControl.toString()
                )
            )
        }
        if (healthScoreResponse?.smoke != null) {
            recommendationList?.add(
                Recommendation(
                    "Smoke Alert",
                    healthScoreResponse.smoke.toString()
                )
            )
        }
        if (healthScoreResponse?.bloodPressureCheck != null) {
            recommendationList?.add(
                Recommendation(
                    "Your Blood Pressur Need Attention",
                    healthScoreResponse.bloodPressureCheck.toString()
                )
            )
        }
    }

    private fun signInWithGoogle() {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso)
        account = GoogleSignIn.getLastSignedInAccount(activity?.baseContext)
        if (account != null) {
            Toast.makeText(this.context, "User already logged in", Toast.LENGTH_LONG).show()
            checkToDisplayDailyDataActivity()
        } else {
            val intent = mGoogleSignInClient!!.signInIntent
            resultLauncher.launch(intent)
        }
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val data: Intent? = result.data
                val task =
                    GoogleSignIn.getSignedInAccountFromIntent(data)

                try {
                    account = task.getResult(ApiException::class.java)
                    Toast.makeText(this.context, "Sig in successful", Toast.LENGTH_LONG).show()
                    if (account != null) {
                        checkToDisplayDailyDataActivity()
                    }
                } catch (e: ApiException) {
                    Log.e("TAG", "signInResult:failed code=" + e.statusCode)
                }
            }
        }

    private fun setSeekBar() {
        mColorSeekBar?.setMaxPosition(100)
        mColorSeekBar?.colorBarPosition = 0 //0 - maxValue
        mColorSeekBar?.isShowAlphaBar = false
        mColorSeekBar?.disabledColor = Color.GRAY
        mColorSeekBar?.setOnColorChangeListener(null)
        mColorSeekBar?.setOnScrollChangeListener(null)
    }
}