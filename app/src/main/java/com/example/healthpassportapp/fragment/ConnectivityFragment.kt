package com.example.healthpassportapp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.healthpassportapp.R
import com.example.healthpassportapp.adapter.ViewPagerDotAdapter
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator

class ConnectivityFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.connectivity_fragment, container, false)

        val dotsIndicator = view.findViewById<DotsIndicator>(R.id.dots_indicator)

        val viewPager2 = view.findViewById<ViewPager2>(R.id.viewPagerMain)
        val list = ArrayList<Int>()
        list.add(R.drawable.medicine1)
        list.add(R.drawable.medicine2)
        list.add(R.drawable.medicine3)
        list.add(R.drawable.medicine4)
        list.add(R.drawable.medicine5)
        list.add(R.drawable.medicine6)
        val adapter = ViewPagerDotAdapter(list)
        viewPager2.adapter = adapter

        // val zoomOutPageTransformer = ZoomOutPageTransformer()
        // viewPager2.setPageTransformer { page, position ->
        //     zoomOutPageTransformer.transformPage(page, position)
        // }

        dotsIndicator.setViewPager2(viewPager2)

    return view
    }


}